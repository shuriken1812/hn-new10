**This is a simple program to fetch top 10 new stories from https://news.ycombinator.com/ written in Rust. For ease of execution I have packaged this as an appimage. Simply clone and run the appimage**

`git clone https://gitlab.com/shuriken1812/hn-new10`

`cd hn-new10`

`chmod +x hn-new10-0.1.0-x86_64.AppImage`

`./hn-new10-0.1.0-x86_64.AppImage`


