extern crate reqwest;
extern crate scraper;

use scraper::{Html, Selector};

fn main() {
    println!("Here are Hacker News's Top 10 New Stories");
    scrape_data("https://news.ycombinator.com/newest")
}

fn scrape_data(url:&str){

    let mut request = reqwest::get(url).unwrap();
    assert!(request.status().is_success());
    let doc_body = Html::parse_document(&request.text().unwrap());

    let by = Selector::parse(".hnuser").unwrap();
    let title = Selector::parse(".titlelink").unwrap();

    let mut topcount = 0;

    let mut array_by: [&str; 10] = [""; 10];
    let mut array_title: [&str; 10] = [""; 10];
    let mut array_url: [&str; 10] = [""; 10];

    for by in doc_body.select(&by){
        let by = by.text().collect::<Vec<_>>();

        if topcount < 10 {
            array_by[topcount] = by[0];
        }else { break; }
        topcount = topcount + 1;
    }

    topcount = 0;

    for title in doc_body.select(&title){
        let title = title.text().collect::<Vec<_>>();

        if topcount < 10 {
            array_title[topcount] = title[0]
        }else { break; }
        topcount = topcount + 1;
    }

    topcount = 0;

    for element in doc_body.select(&title){

        let url = match element.value().attr("href") {Some (target_url)=> target_url, _ => "no url found"};

        if topcount < 10 {
            array_url[topcount] = url
        }else { break; }
        topcount = topcount + 1;
    }

    topcount = 0;

    while topcount < 10 {
        println!("{} | {} | {} ", array_title[topcount], array_by[topcount] , array_url[topcount]);
        topcount = topcount + 1;
    }

}
